# Ignore Formatting sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DocBook-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DocBook-Compare-5_2_2_j/samples/sample-name`.*

---

## Running the sample via the Ant build script

If you have Ant installed, use the build script provided to run the DocBook Compare sample. Simply type the following command to run the comparison and produce the output file *docbook-article-revision.xml*.

	ant run
	
If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line

It is possible to compare two DocBook files using the command line tool from sample directory as follows.

    java -jar ../../deltaxml-docbook.jar compare docbook-article-version-1.xml docbook-article-version-2.xml docbook-article-revision.xml indent=yes ignore-inline-formatting=true
    
The docbook-article-revision.xml file contains the result, which does not show the revisions where inline emphasis markup has been inserted as ignore-inline-formatting is set to true. The author markup inside the emphasis which was inserted around the text Fred in docbook-article-version-2.xml is shown as a change because it is NOT an inline fomatting element change. 

Other parameters are available as discussed in the [User Guide](https://docs.deltaxml.com/docbook-compare/latest/user-guide) and summarised by the following command.

    java -jar ../../deltaxml-docbook.jar describe
   

To clean up the sample directory, run the following Ant command.

	ant clean